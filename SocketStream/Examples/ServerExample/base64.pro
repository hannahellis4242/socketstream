TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11

SOURCES += test.cpp \
    base64.cpp

INCLUDEPATH += ../../../ext/gmock-1.7.0/include
INCLUDEPATH += ../../../ext/gmock-1.7.0/gtest/include

LIBS += ../../../ext/gmock-1.7.0/lib/.libs/libgmock_main.a
LIBS += ../../../ext/gmock-1.7.0/lib/.libs/libgmock.a
LIBS += ../../../ext/gmock-1.7.0/gtest/lib/.libs/libgtest.a
LIBS += -lpthread

HEADERS += base64.h
