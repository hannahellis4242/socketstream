/*
    Copyright (C) 2016  Adam Ellis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SocketStream.h"
#include "SocketException.h"
#include <thread>
#include <iostream>

#include "ServerSocket.h"

void getline(std::istream &is,std::string &str)
{
	while(char c=is.get())
	{
		str.push_back(c);
	}
}

void sayHello()
{
	try
	{
//		ServerSocket sock(SOCK_STREAM,4481);
//        auto &s=SocketStream(&sock.accept(),&sock);
//		std::cout<<"connection established"<<std::endl;
//		s<<"Hello World"<<std::endl;
//		auto msg=std::string();
//		getline(s,msg);
//		std::cout<<msg<<std::endl;
//		msg.clear();
//		getline(s,msg);
//		std::cout<<msg<<std::endl;
	}
	catch(const SocketException &e)
	{
		std::cerr<<e.what()<<std::endl;
	}

}

int main()
{

	std::thread t(sayHello);
	std::cout<<"Press enter to close"<<std::endl;
	std::cin.ignore();
	t.detach();
	return 0;
}

