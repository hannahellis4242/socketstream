/*
    Copyright (C) 2016  Adam Ellis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SocketStream.h"
#include "SocketException.h"
#include <iostream>

void getline(std::istream &is,std::string &str)
{
	while(char c=is.get())
	{
		str.push_back(c);
	}
}

int main(int argc,char *argv[])
{
	if(argc!=3)
	{
		std::cout<<"usage ClientExample <host> <port>"<<std::endl;
		return 1;
	}
	try
	{
		SocketStream ss(argv[1],atoi(argv[2]));
		auto str=std::string();
		getline(ss,str);
		std::cout<<str<<std::endl;
	}
	catch(const SocketException &e)
	{
		std::cout<<"caught exception"<<std::endl;
		std::cerr<<e.what()<<std::endl;
	}
	catch(...)
	{
		std::cout<<"caught a different exception"<<std::endl;
		throw;
	}

	return 0;
}
