TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11

SOURCES += main.cpp

unix:!macx: LIBS += -L$$OUT_PWD/../../SocketStream/ -lSocketStream

INCLUDEPATH += $$PWD/../../SocketStream
DEPENDPATH += $$PWD/../../SocketStream

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../SocketStream/libSocketStream.a
LIBS += -L../../../ext/boost/lib
LIBS += -lboost_regex
LIBS += -lssl -lcrypto
