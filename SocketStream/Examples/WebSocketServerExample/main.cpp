/*
    Copyright (C) 2016  Adam Ellis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ServerSocket.h"
#include "WebSocket.h"
#include "SocketException.h"
#include <iostream>
#include <thread>

void sayHello()
{
    try
    {
        std::cout<<"listening on port 4481"<<std::endl;
        auto sock=ServerSocket(SOCK_STREAM,4481);
        const auto &connection=sock.acceptWebSocket();
        const auto msg = connection.read() ;
        std::cout<<msg<<std::endl;
        connection.write(msg);
    }
    catch(const SocketException &e)
    {
        std::cerr<<e.what()<<std::endl;
    }
}

int main()
{
    std::thread t(sayHello);
    std::cout<<"Press enter to close"<<std::endl;
    std::cin.ignore();
    t.detach();
    return 0;
}
