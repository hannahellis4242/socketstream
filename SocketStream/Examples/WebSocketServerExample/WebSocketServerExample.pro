TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11

SOURCES += main.cpp

unix:!macx: LIBS += -L$$OUT_PWD/../../SocketStream/ -lSocketStream

INCLUDEPATH += $$PWD/../../SocketStream
DEPENDPATH += $$PWD/../../SocketStream

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../SocketStream/libSocketStream.a

LIBS += -pthread
LIBS += -L../../../ext/boost/lib
LIBS += $$PWD/../../../ext/boost/lib/libboost_regex.a
LIBS += -lssl -lcrypto
