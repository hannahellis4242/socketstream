#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "DataFrame.h"

using namespace testing;

TEST(testDataFrame,canExtractInfoFromBinary)
{
    const auto dataframe=DataFrame(Binary(std::vector<bool>{1,0,0,0,0,0,0,1,1,0,0,0,1,1,1,1,0,1,0,1,1,1,0,0,0,0,1,1,1,1,0,0,
                                                            0,0,1,1,1,0,1,0,0,1,1,0,0,0,1,1,0,0,0,1,0,0,0,1,0,1,0,1,1,0,0,1,
                                                            0,1,0,0,1,0,0,1,0,0,0,1,0,0,0,0,0,0,1,1,1,1,0,1,0,1,0,1,1,0,1,1,
                                                            0,1,0,1,1,1,1,1,0,1,0,0,0,0,1,1,0,0,1,0,1,0,0,0,0,1,0,1,0,0,1,1,
                                                            0,0,0,1,1,0,1,0,0,0,0,1,0,0,0,0,0,0,1,1,1,0,0,1,0,1,0,1,0,0,1,0,
                                                            0,1,0,1,1,1,1,0}));

    EXPECT_THAT(dataframe.payload(),Eq("Message to send"));
}

Binary nullGenerator()
{
    return Binary(std::vector<bool>{0,1,0,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,0,1,0,0,1,1,0,0,0,1,1});
}

TEST(testDataFrame,canCreateFrameForStringPayload)
{
    const auto dataFrame=DataFrame("Message to send",nullGenerator);
    EXPECT_THAT(dataFrame.raw(),Eq(Binary(std::vector<bool>{1,0,0,0,0,0,0,1,1,0,0,0,1,1,1,1,0,1,0,1,1,1,0,0,0,0,1,1,1,1,0,0,
                                                            0,0,1,1,1,0,1,0,0,1,1,0,0,0,1,1,0,0,0,1,0,0,0,1,0,1,0,1,1,0,0,1,
                                                            0,1,0,0,1,0,0,1,0,0,0,1,0,0,0,0,0,0,1,1,1,1,0,1,0,1,0,1,1,0,1,1,
                                                            0,1,0,1,1,1,1,1,0,1,0,0,0,0,1,1,0,0,1,0,1,0,0,0,0,1,0,1,0,0,1,1,
                                                            0,0,0,1,1,0,1,0,0,0,0,1,0,0,0,0,0,0,1,1,1,0,0,1,0,1,0,1,0,0,1,0,
                                                            0,1,0,1,1,1,1,0})));
}
