TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11

SOURCES += main.cpp

INCLUDEPATH += ../../../ext/gmock-1.7.0/include
INCLUDEPATH += ../../../ext/gmock-1.7.0/gtest/include

LIBS += ../../../ext/gmock-1.7.0/lib/.libs/libgmock_main.a
LIBS += ../../../ext/gmock-1.7.0/lib/.libs/libgmock.a
LIBS += ../../../ext/gmock-1.7.0/gtest/lib/.libs/libgtest.a
LIBS += -lpthread

unix:!macx: LIBS += -L$$OUT_PWD/../../SocketStream/ -lSocketStream

INCLUDEPATH += $$PWD/../../SocketStream
DEPENDPATH += $$PWD/../../SocketStream

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../../SocketStream/libSocketStream.a
