#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "Binary.h"

using namespace testing;

TEST(testBinary,defaultConstructor)
{
    const auto bin=Binary();
    EXPECT_THAT(bin.get(),IsEmpty());
}

TEST(testBinary,constructWithChar)
{
    const auto bin=Binary('a');
    EXPECT_THAT(bin.get(),ContainerEq(std::vector<bool>{false,true,true,false,false,false,false,true}));
}

TEST(testBinary,constructWithVectorOfBool)
{
    const auto bin=Binary(std::vector<bool>{true,false});
    EXPECT_THAT(bin.get(),ContainerEq(std::vector<bool>{true,false}));
}

TEST(testBinary,constructWithUint16t)
{
    const auto bin=Binary(uint16_t(0b0001000100010001));
    EXPECT_THAT(bin.get(),Eq(std::vector<bool>{0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1}));
}

TEST(testBinary,constructWithUint32t)
{
    const auto bin=Binary(uint32_t(0b00010001000100010001000100010001));
    EXPECT_THAT(bin.get(),Eq(std::vector<bool>{0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1}));
}

TEST(testBinary,constructWithUint64t)
{
    const auto bin=Binary(uint64_t(0b0001000100010001000100010001000100010001000100010001000100010001));
    EXPECT_THAT(bin.get(),Eq(std::vector<bool>{0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1}));
}

TEST(testBinary,convertToChar)
{
    const auto bin=Binary('A');
    EXPECT_THAT(bin.get(),ContainerEq(std::vector<bool>{false,true,false,false,false,false,false,true}));
    EXPECT_THAT((char)bin,Eq('A'));
}

TEST(testBinary,convertToInt)
{
    const auto bin=Binary(int(42));
    EXPECT_THAT(bin.get(),Eq(std::vector<bool>{ false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, true, false }));
    EXPECT_THAT((int)bin,Eq(42));
}

TEST(testBinary,convertToCharFromVector)
{
    const auto bin=Binary(std::vector<bool>{true,false,false,false,false,false,true});
    EXPECT_THAT((char)bin,Eq('A'));
}

TEST(testBinary,convertToCharFromVectorWhenBinaryIsLargerThanChar)
{
    const auto bin=Binary(std::vector<bool>{false,false,true,false,false,false,false,false,true});
    EXPECT_THAT((char)bin,Eq(0x00));
}

TEST(testBinary,testEquality)
{
    const auto bin=Binary('a');
    EXPECT_THAT(bin==Binary('b'),Eq(false));
    EXPECT_THAT(bin==Binary('a'),Eq(true));
    EXPECT_THAT(bin==Binary(std::vector<bool>{false,true,true,false,false,false,false,true}),Eq(true));
    EXPECT_THAT(bin==Binary(std::vector<bool>{false,false,true,true,false,false,false,false,true}),Eq(false));
    EXPECT_THAT(bin==Binary(std::vector<bool>{true,true,false,false,false,false,true}),Eq(false));
}

TEST(testBinary,canMakeANewBinaryFromASubSectionOfBinary)
{
    const auto bin=Binary(std::vector<bool>{0,1,1,0,0,0,0,1,0,1,1,0,0,0,1,0});
    EXPECT_THAT(bin.sub(4,8),Eq(Binary(std::vector<bool>{0,0,0,1})));
}

TEST(testBinary,concatanateTwoBinarys)
{
    const auto bin1=Binary(std::vector<bool>{true,false});
    const auto bin2=Binary(std::vector<bool>{false,true});
    EXPECT_THAT(bin1+bin2,Eq(Binary(std::vector<bool>{true,false,false,true})));
}

TEST(testBinary,stringToBinary)
{
    const auto str=std::string("Hello World");
    EXPECT_THAT(convertToBinary(str),Eq(Binary(std::vector<bool>{0,1,0,0,1,0,0,0,0,1,1,0,0,1,0,1,0,1,1,0,1,1,0,0,0,1,1,0,1,1,0,0,0,1,1,0,1,1,1,1,0,0,1,0,0,0,0,0,0,1,0,1,0,1,1,1,0,1,1,0,1,1,1,1,0,1,1,1,0,0,1,0,0,1,1,0,1,1,0,0,0,1,1,0,0,1,0,0})));
}

TEST(testBinary,xor)
{
    auto bin=convertToBinary(std::string("Hello"));
    const auto mask=Binary('a');
    bin^=mask;
    EXPECT_THAT(bin.get(),Eq(std::vector<bool>{0,0,1,0,1,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,1,1,0,1,0,0,0,0,1,1,0,1,0,0,0,0,1,1,1,0}));
}

TEST(testBinary,convertToString)
{
    const auto bin=Binary(std::vector<bool>{0,1,0,0,1,0,0,0,0,1,1,0,0,1,0,1,0,1,1,0,1,1,0,0,0,1,1,0,1,1,0,0,0,1,1,0,1,1,1,1,0,0,1,0,0,0,0,0,0,1,0,1,0,1,1,1,0,1,1,0,1,1,1,1,0,1,1,1,0,0,1,0,0,1,1,0,1,1,0,0,0,1,1,0,0,1,0,0});
    EXPECT_THAT(convertToString(bin),Eq("Hello World"));
}
