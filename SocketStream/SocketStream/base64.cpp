/*
    Copyright (C) 2016  Adam Ellis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "base64.h"

namespace base64
{
static const char base64Alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static int decodedLength(const std::string &in);
static int encodedLength(size_t length);
static int encodedLength(const std::string &in);
static int encodedLength(const std::vector<uint8_t> &in);
static void a3_to_a4(unsigned char * a4, unsigned char * a3);
static void a4_to_a3(unsigned char * a3, unsigned char * a4);
static unsigned char b64_lookup(unsigned char c);
template<class In>
static bool encode(const In &in, std::string &out);
template<class Out>
static bool decode(const std::string &in, Out &out);

template<class In>
static bool encode(const In &in, std::string &out)
{
	int i = 0, j = 0;
	size_t enc_len = 0;
	unsigned char a3[3];
	unsigned char a4[4];

	out.resize(encodedLength(in));

	int input_len = in.size();
	auto input = in.begin();

	while (input_len--)
	{
		a3[i++] = *(input++);
		if (i == 3) {
			a3_to_a4(a4, a3);

			for (i = 0; i < 4; i++)
			{
				out[enc_len++] = base64Alphabet[a4[i]];
			}

			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
		{
			a3[j] = '\0';
		}

		a3_to_a4(a4, a3);

		for (j = 0; j < i + 1; j++)
		{
			out[enc_len++] = base64Alphabet[a4[j]];
		}

		while ((i++ < 3))
		{
			out[enc_len++] = '=';
		}
	}

	return (enc_len == out.size());
}

template<class Out>
static bool decode(const std::string &in, Out &out)
{
	int i = 0, j = 0;
	size_t dec_len = 0;
	unsigned char a3[3];
	unsigned char a4[4];

	int input_len = in.size();
	auto input = in.begin();

	out.resize(decodedLength(in));

	while (input_len--)
	{
		if (*input == '=')
		{
			break;
		}

		a4[i++] = *(input++);
		if (i == 4)
		{
			for (i = 0; i <4; i++)
			{
				a4[i] = b64_lookup(a4[i]);
			}

			a4_to_a3(a3,a4);

			for (i = 0; i < 3; i++)
			{
				out[dec_len++] = a3[i];
			}

			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 4; j++)
		{
			a4[j] = '\0';
		}
		for (j = 0; j < 4; j++)
		{
			a4[j] = b64_lookup(a4[j]);
		}

		a4_to_a3(a3,a4);

		for (j = 0; j < i - 1; j++)
		{
			out[dec_len++] = a3[j];
		}
	}

	return (dec_len == out.size());
}

static int decodedLength(const std::string &in)
{
	int numEq = 0;
	int n = in.size();

	for (std::string::const_reverse_iterator it = in.rbegin(); *it == '='; ++it) {
		++numEq;
	}

	return ((6 * n) / 8) - numEq;
}

static int encodedLength(size_t length)
{
	return (length + 2 - ((length + 2) % 3)) / 3 * 4;
}

static int encodedLength(const std::string &in)
{
	return encodedLength(in.length());
}

static int encodedLength(const std::vector<uint8_t> &in)
{
	return encodedLength(in.size());
}

void stripPadding(std::string &in)
{
	while (!in.empty() && *(in.rbegin()) == '=') in.resize(in.size() - 1);
}

static void a3_to_a4(unsigned char *a4, unsigned char *a3)
{
	a4[0] = (a3[0] & 0xfc) >> 2;
	a4[1] = ((a3[0] & 0x03) << 4) + ((a3[1] & 0xf0) >> 4);
	a4[2] = ((a3[1] & 0x0f) << 2) + ((a3[2] & 0xc0) >> 6);
	a4[3] = (a3[2] & 0x3f);
}

static void a4_to_a3(unsigned char *a3, unsigned char *a4)
{
	a3[0] = (a4[0] << 2) + ((a4[1] & 0x30) >> 4);
	a3[1] = ((a4[1] & 0xf) << 4) + ((a4[2] & 0x3c) >> 2);
	a3[2] = ((a4[2] & 0x3) << 6) + a4[3];
}

static unsigned char b64_lookup(unsigned char c)
{
	if(c >='A' && c <='Z') return c - 'A';
	if(c >='a' && c <='z') return c - 71;
	if(c >='0' && c <='9') return c + 4;
	if(c == '+') return 62;
	if(c == '/') return 63;
	return 255;
}

const char *Base64Exception::what() const throw()
{
	return "base64 error";
}

std::string encode(const std::string &in)throw()
{
	auto out=std::string();
	if(!encode(in,out))
	{
		throw Base64Exception();
	}
	return out;
}

std::string decodeToString(const std::string &in)
{
	auto out=std::string();
	if(!decode(in,out))
	{
		throw Base64Exception();
	}
	return out;
}

std::string encode(const std::vector<uint8_t> &in)
{
	auto out=std::string();
	if(!encode(in,out))
	{
		throw Base64Exception();
	}
	return out;
}

std::vector<uint8_t> decodeToNumber(const std::string &in)
{
	auto out=std::vector<uint8_t>();
	if(!decode(in,out))
	{
		throw Base64Exception();
	}
	return out;
}

}
