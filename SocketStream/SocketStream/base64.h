/*
    Copyright (C) 2016  Adam Ellis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BASE64_H
#define BASE64_H

#include <string>
#include <vector>
#include <exception>

namespace base64
{
class Base64Exception : public std::exception
{
	public:
		const char* what() const throw();
};

std::string encode(const std::string &in) throw();
std::string encode(const std::vector<uint8_t> &in);
std::string decodeToString(const std::string &in);
std::vector<uint8_t> decodeToNumber(const std::string &in);
void stripPadding(std::string &in);
}

#endif // BASE64_H
