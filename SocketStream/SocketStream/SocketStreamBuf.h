/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SOCKETSTREAMBUF_H
#define SOCKETSTREAMBUF_H

#include <streambuf>
#include <vector>
#include "SocketBase.h"

//#define DEBUG

class SocketStreamBuf : public std::streambuf
{
	private:
		const SocketBase & socket_;
		const unsigned int bufSize_;
		const unsigned int pbackSize_;
		std::vector<char_type> gBuffer;
		std::vector<char_type> pBuffer;
		char_type * pbEnd ;

		SocketStreamBuf(const SocketStreamBuf &);
		SocketStreamBuf &operator=(const SocketStreamBuf &);

		void bufferOut();
		void bufferIn();
	public:
		SocketStreamBuf(const SocketBase &socket,const unsigned int bufSize=1024,const unsigned int pbackSize=512);
		~SocketStreamBuf();
		int_type overflow(int_type c);
		int sync();
		int_type underflow();
		int_type pbackfail(int_type c);

#ifdef DEBUG
		std::string getBufferState() const ;
		std::string putBufferState() const ;
#endif
};

#endif // SOCKETSTREAMBUF_H
