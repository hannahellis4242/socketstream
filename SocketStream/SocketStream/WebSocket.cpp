/*
    Copyright (C) 2016  Adam Ellis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "WebSocket.h"
#include "SocketException.h"
#include "boost/regex.hpp"
#include "base64.h"
#include "DataFrame.h"
#include <openssl/sha.h>
#include <sstream>
#include <random>

static std::string extractKey(const std::string &handshake)
{
    auto r=boost::regex("Sec-WebSocket-Key: (\\S+)");

    auto it=boost::sregex_token_iterator(handshake.begin(),handshake.end(),r,0);
    auto matches=std::vector<std::string>();
    for(auto endIt=boost::sregex_token_iterator();it!=endIt;++it)
    {
        matches.push_back(*it);
    }

    auto key=std::string();
    for(const auto match:matches)
    {
        auto groups=boost::smatch();
        //attempt to catch the key for the matched string
        boost::regex_search(match,groups,r);
        if(!groups.empty())
        {
            key=groups[1];
            break;
        }
    }

    if(key.empty())
    {
        throw SocketException("Could not exctract key from websocket handshake");
    }
    return key;
}

static std::string response(const std::string &key)
{
    const auto specialString=key+std::string("258EAFA5-E914-47DA-95CA-C5AB0DC85B11");
    const auto str = std::vector<unsigned char>(specialString.begin(),specialString.end());
    unsigned char hash[SHA_DIGEST_LENGTH]; // == 20
    SHA1(&str[0],str.size(),hash);
    auto responekey=base64::encode(std::vector<uint8_t>(hash,hash+SHA_DIGEST_LENGTH));

    std::stringstream ss ;
    ss<<"HTTP/1.1 101 Switching Protocols\r\n";
    ss<<"Upgrade: WebSocket\r\n";
    ss<<"Connection: Upgrade\r\n";
    ss<<"Sec-WebSocket-Accept: "<<responekey<<"\r\n";
    ss<<"Sec-WebSocket-Protocol: null\r\n\r\n";
    return ss.str();
}

static std::string rawRead(const int fd,const unsigned int bytes)
{
    char *buffer=new char[bytes];
    auto bytesRecv=recv(fd,buffer,bytes,0);
    if(bytesRecv<=0)
    {
        throw SocketException("could not read from socket");
    }
    auto out=std::string(buffer,buffer+bytesRecv);
    delete[] buffer;
    return out;
}

std::vector<bool> toBin(const char *begin,const char *end)
{
    auto out=std::vector<bool>();
    for(auto it=begin;it!=end;++it)
    {
        auto mask=0x80;
        while(mask>0x00)
        {
            out.push_back((mask&*it)>0);
            mask=mask>>1;
        }
    }
    return out;
}

static std::vector<bool> rawReadToBin(const int fd,const unsigned int bytes)
{
    char *buffer=new char[bytes];
    auto bytesRecv=recv(fd,buffer,bytes,0);
    if(bytesRecv<=0)
    {
        throw SocketException("could not read from socket");
    }
    auto out=toBin(buffer,buffer+bytesRecv);
    delete[] buffer;
    return out;
}

static void rawWrite(const int fd,const std::string &msg)
{
    auto bytesSent=send(fd,msg.c_str(),msg.size(),0);

    if(bytesSent==-1)
    {
        throw SocketException("could not write to socket");
    }
}

std::default_random_engine generator;

static Binary random32BitNum()
{
    auto num=uint32_t(generator());
    return Binary(num);
}

WebSocket::WebSocket(const int fd):Socket(fd)
{
    //handle handshake
    const auto handshake=rawRead(fd_,4096);
    rawWrite(fd_,response(extractKey(handshake)));
}

WebSocket::~WebSocket()
{

}

std::string WebSocket::read(const unsigned int bytes) const
{
    return DataFrame(rawReadToBin(fd_,bytes)).payload();
}

void WebSocket::write(const std::string &message) const
{
    rawWrite(fd_,convertToString(DataFrame(message,random32BitNum).raw()));
}

