/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SocketStreamBuf.h"
#include <algorithm>

#ifdef DEBUG
#include <sstream>
#include <iostream>
#endif

void SocketStreamBuf::bufferOut()
{
	if(pptr()>pbase())
	{
		socket_.write(std::string(pbase(),pptr()));
		auto charsInBuffer=pptr()-pbase();
		//push pptr back to pbase
		pbump(-charsInBuffer);
	}
}

void SocketStreamBuf::bufferIn()
{
	auto start=gptr();
	if(start<eback())
	{
		//we have putback characters to move
		start=std::move(gptr(),eback(),eback());
	}
	else
	{
		start=eback();
	}
	//read enough characters to fill the buffer
	const auto numCharsToRead=egptr()-start;
	if(numCharsToRead>0)
	{
		const auto & data=socket_.read(numCharsToRead);
		start=std::copy(data.begin(),data.end(),start);
	}
	setg(eback(),eback(),egptr());
#ifdef DEBUG
	std::cout << __FUNCTION__ << std::endl ;
	std::cout << getBufferState() << std::endl ;
#endif
}

SocketStreamBuf::SocketStreamBuf(const SocketBase &socket, const unsigned int bufSize, const unsigned int pbackSize)
	:socket_(socket),bufSize_(bufSize),pbackSize_(pbackSize),gBuffer(bufSize_+pbackSize_,'\0'),pBuffer(bufSize_,'\0')
{
	pbEnd=&gBuffer[0];
	auto gStart=&gBuffer[pbackSize_];
	auto gEnd=gStart+bufSize_;
	setg(gStart,gEnd,gEnd);

	auto pStart=&pBuffer[0];
	auto pEnd=pStart+bufSize_;
	setp(pStart,pEnd);
}

SocketStreamBuf::~SocketStreamBuf()
{
	//make sure we empty the put buffer before we die
	sync();
	gBuffer.clear();
	pBuffer.clear();
}

std::streambuf::int_type SocketStreamBuf::overflow(std::streambuf::int_type c)
{
	bufferOut();
	return traits_type::eq_int_type(c,traits_type::eof()) ? traits_type::not_eof(c) : sputc(c);
}

int SocketStreamBuf::sync()
{
	bufferOut();
	return traits_type::int_type(0);
}

std::streambuf::int_type SocketStreamBuf::underflow()
{
	if(gptr()<egptr())
	{
		return traits_type::to_int_type(*gptr());
	}

	bufferIn();
	return traits_type::to_int_type(*gptr());
}

std::streambuf::int_type SocketStreamBuf::pbackfail(std::streambuf::int_type c)
{
	if(gptr()!=eback()-pbackSize_)
	{
		gbump(-1);
		if(!traits_type::eq_int_type(c,traits_type::eof()))
		{
			*gptr()=traits_type::to_char_type(c);
		}
		return traits_type::not_eof(c);
	}
	return traits_type::eof();
}

#ifdef DEBUG
std::string SocketStreamBuf::getBufferState() const
{
	std::stringstream ss;
	ss<<"gBuffer"<<std::endl;
	for(const auto &b:gBuffer)
	{
		ss<<(void*)&b<<" : "<<b;
		if(&b==eback())
		{
			ss<<"  <-eback";
		}
		if(&b==gptr())
		{
			ss<<"  <-gptr";
		}
		if(&b==egptr())
		{
			ss<<"  <-egptr";
		}
		ss<<std::endl;
	}
	ss<<std::endl;
	ss<<"eback() : "<<(void*)eback()<<std::endl;
	ss<<"gptr : "<<(void*)gptr()<<std::endl;
	ss<<"egptr : "<<(void*)egptr()<<std::endl;
	return ss.str();
}
#endif
