/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#include "Socket.h"
#include <vector>
#include <memory>

class WebSocket;

class ServerSocket:public Socket
{
    protected:
        std::vector<std::unique_ptr<Socket>> connections_;
	public:
		ServerSocket(const int streamType,const unsigned int port);
        ServerSocket(const ServerSocket &)=delete;
        ServerSocket operator=(const ServerSocket &)=delete;
        ServerSocket(ServerSocket &&)=default;
        ServerSocket &operator=(ServerSocket &&)=default;
        virtual ~ServerSocket();
        Socket & accept();
        WebSocket & acceptWebSocket();
        void disconnect(const Socket &);
};

#endif // SERVERSOCKET_H
