#include "Binary.h"


Binary::Binary(const std::vector<bool> &bin):bin_(bin){}

Binary::Binary(const char c)
{
    bin_.reserve(8);
    auto mask=uint8_t(0x80);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

Binary::Binary(const uint8_t c)
{
    bin_.reserve(8);
    auto mask=uint8_t(0x80);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

Binary::Binary(const uint16_t c)
{
    bin_.reserve(16);
    auto mask=uint16_t(0x8000);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

Binary::Binary(const uint32_t c)
{
    bin_.reserve(32);
    auto mask=uint32_t(0x80000000);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

Binary::Binary(const uint64_t c)
{
    bin_.reserve(64);
    auto mask=uint64_t(0x8000000000000000);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

Binary::Binary(const int8_t c)
{
    bin_.reserve(8);
    auto mask=uint8_t(0x80);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

Binary::Binary(const int16_t c)
{
    bin_.reserve(16);
    auto mask=uint16_t(0x8000);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

Binary::Binary(const int32_t c)
{
    bin_.reserve(32);
    auto mask=uint32_t(0x80000000);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

Binary::Binary(const int64_t c)
{
    bin_.reserve(64);
    auto mask=uint64_t(0x8000000000000000);
    while(mask>0)
    {
        bin_.push_back((mask&c)>0);
        mask=mask>>1;
    }
}

bool Binary::operator==(const Binary &rhs) const
{
    if(bin_.size()==rhs.bin_.size())
    {
        auto lhsIt=bin_.begin();
        auto rhsIt=rhs.bin_.begin();
        const auto lhsItEnd=bin_.end();
        const auto rhsItEnd=rhs.bin_.end();
        while((lhsIt!=lhsItEnd)||(rhsIt!=rhsItEnd))
        {
            if(*lhsIt!=*rhsIt)
            {
                return false;
            }
            ++lhsIt;
            ++rhsIt;
        }
        return true;
    }
    return false;
}

bool Binary::operator[](const unsigned int i) const
{
    return bin_[i];
}

unsigned int Binary::bits() const
{
    return bin_.size();
}

const std::vector<bool> &Binary::get() const
{
    return bin_;
}

Binary Binary::sub(const unsigned int from, const unsigned int to) const
{
    if((from<=bin_.size())&&(to<=bin_.size())&&(from<to))
    {
        return Binary(std::vector<bool>(bin_.begin()+from,bin_.begin()+to));
    }
    return Binary();
}

Binary Binary::operator+(const Binary &rhs) const
{
    return Binary(bin_)+=rhs;
}

const Binary &Binary::operator+=(const Binary &rhs)
{
    bin_.insert(bin_.end(),rhs.bin_.begin(),rhs.bin_.end());
    return *this;
}

const Binary &Binary::operator&=(const Binary &rhs)
{
    auto rhsIt=rhs.bin_.begin();
    const auto endRhsIt=rhs.bin_.end();
    for(auto it=bin_.begin(),endIt=bin_.end();it!=endIt;++it)
    {
        *it=*it&&*rhsIt;
        rhsIt=(rhsIt+1==endRhsIt)?rhs.bin_.begin():rhsIt+1;
    }
    return *this;
}

const Binary &Binary::operator|=(const Binary &rhs)
{
    auto rhsIt=rhs.bin_.begin();
    const auto endRhsIt=rhs.bin_.end();
    for(auto it=bin_.begin(),endIt=bin_.end();it!=endIt;++it)
    {
        *it=*it||*rhsIt;
        rhsIt=(rhsIt+1==endRhsIt)?rhs.bin_.begin():rhsIt+1;
    }
    return *this;
}

const Binary &Binary::operator^=(const Binary &rhs)
{
    auto rhsIt=rhs.bin_.begin();
    const auto endRhsIt=rhs.bin_.end();
    for(auto it=bin_.begin(),endIt=bin_.end();it!=endIt;++it)
    {
        *it=*it!=*rhsIt;
        rhsIt=(rhsIt+1==endRhsIt)?rhs.bin_.begin():rhsIt+1;
    }
    return *this;
}

Binary::operator int64_t() const
{
    auto out=uint64_t(0x00);
    auto mask=uint64_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0;
        mask=mask>>1;
    }
    return out;
}

Binary::operator int32_t() const
{
    auto out=uint32_t(0x00);
    auto mask=uint32_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0x00;
        mask=mask>>1;
    }
    return out;
}

Binary::operator int16_t() const
{
    auto out=uint16_t(0x00);
    auto mask=uint16_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0;
        mask=mask>>1;
    }
    return out;
}

Binary::operator int8_t() const
{
    auto out=uint8_t(0x00);
    auto mask=uint8_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0x00;
        mask=mask>>1;
    }
    return out;
}

Binary::operator uint64_t() const
{
    auto out=uint64_t(0x00);
    auto mask=uint64_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0;
        mask=mask>>1;
    }
    return out;
}

Binary::operator uint32_t() const
{
    auto out=uint32_t(0x00);
    auto mask=uint32_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0x00;
        mask=mask>>1;
    }
    return out;
}

Binary::operator uint16_t() const
{
    auto out=uint16_t(0x00);
    auto mask=uint16_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0;
        mask=mask>>1;
    }
    return out;
}

Binary::operator uint8_t() const
{
    auto out=uint8_t(0x00);
    auto mask=uint8_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0x00;
        mask=mask>>1;
    }
    return out;
}

Binary::operator char() const
{
    auto out=uint8_t(0x00);
    auto mask=uint8_t(0x01<<(bin_.size()-1));
    for(const auto bit:bin_)
    {
        out|=bit?mask:0x00;
        mask=mask>>1;
    }
    return out;
}

std::ostream &operator<<(std::ostream &os, const Binary &b)
{
    for(const auto &bit:b.get())
    {
        os<<(bit?1:0);
    }
    return os;
}

std::string convertToString(const Binary &bin)
{
    auto out=std::string();
    if(bin.bits()%8==0)
    {
        for(unsigned int i=0;i<bin.bits();i+=8)
        {
            const auto subBin=bin.sub(i,i+8);
            out.push_back(subBin);
        }
    }
    return out;
}
