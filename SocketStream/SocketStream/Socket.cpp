/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Socket.h"
#include "SocketException.h"
#include <sstream>
#include <netdb.h>
#include <unistd.h> //sockets close
#include <arpa/inet.h> //for inet_ntop

Socket::Socket(int s):fd_(s){}

Socket::Socket(int s, const sockaddr & in):fd_(s),add_(in){}

Socket::~Socket()
{
	close(fd_);
}

std::string Socket::address() const
{
	if(add_.sa_family==AF_INET)
	{
		char s[INET_ADDRSTRLEN];
		inet_ntop(add_.sa_family,add_.sa_data,s,INET_ADDRSTRLEN);
		return std::string(s);
	}
	else if(add_.sa_family==AF_INET6)
	{
		char s[INET6_ADDRSTRLEN];
		inet_ntop(add_.sa_family,add_.sa_data,s,INET6_ADDRSTRLEN);
		return std::string(s);
	}
	return std::string();
}

unsigned int Socket::port() const
{
	if(add_.sa_family==AF_INET)
	{
		auto ipv4Add=(const sockaddr_in *)(&add_);
		return ntohs(ipv4Add->sin_port);
	}
	else if(add_.sa_family==AF_INET6)
	{
		auto ipv6Add=(const sockaddr_in6 *)(&add_);
		return ntohs(ipv6Add->sin6_port);
	}
	return 0;
}

std::string Socket::read(const unsigned int bytes) const
{
	char *buffer=new char[bytes];
	auto bytesRecv=recv(fd_,buffer,bytes,0);
	if(bytesRecv<=0)
	{
		throw SocketException("could not read from socket");
	}
    auto out=std::string(buffer,buffer+bytesRecv);
    delete[] buffer;
    return out;
}

void Socket::write(const std::string &message) const
{
	auto bytesSent=send(fd_,message.c_str(),message.size(),0);

	if(bytesSent==-1)
	{
		throw SocketException("could not write to socket");
	}
}

std::ostream &operator<<(std::ostream &os, const Socket &s)
{
	os<<s.read();
	return os;
}

std::istream &operator>>(std::istream &is, const Socket &s)
{
	auto buf=std::string();
	is>>buf;
	s.write(buf);
	return is;
}
