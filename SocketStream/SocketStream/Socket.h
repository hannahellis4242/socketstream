/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SOCKET_H
#define SOCKET_H

#include "SocketBase.h"
#include <netdb.h>

class Socket : public SocketBase
{
	protected:
		int fd_;
		sockaddr add_;
	public:
		Socket(int);
		Socket(int,const sockaddr &);
        Socket(const Socket &)=delete;
        Socket &operator=(const Socket &)=delete;
        Socket(Socket &&)=default;
        Socket &operator=(Socket &&)=default;
		virtual ~Socket();
		std::string address() const;
		unsigned int port() const;
        virtual std::string read(const unsigned int bytes=1024) const;
        virtual void write(const std::string & message) const;
};

std::ostream & operator<<(std::ostream &os,const Socket &s);
std::istream & operator>>(std::istream & is,const Socket &s);

#endif // SOCKET_H
