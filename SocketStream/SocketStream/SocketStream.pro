#-------------------------------------------------
#
# Project created by QtCreator 2016-07-18T21:07:21
#
#-------------------------------------------------

QT       -= core gui

TARGET = SocketStream
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++11

SOURCES += ClientSocket.cpp ServerSocket.cpp Socket.cpp SocketException.cpp SocketStreamBuf.cpp SocketStream.cpp WebSocket.cpp base64.cpp \
    Binary.cpp \
    DataFrame.cpp

HEADERS += ClientSocket.h ServerSocket.h SocketBase.h SocketException.h Socket.h SocketStreamBuf.h SocketStream.h WebSocket.h base64.h \
    Binary.h \
    DataFrame.h


unix:!macx: LIBS += -L$$PWD/../../ext/boost/lib/ -lboost_regex
LIBS += -lssl -lcrypto

INCLUDEPATH += $$PWD/../../ext/boost/include
DEPENDPATH += $$PWD/../../ext/boost/include

unix:!macx: PRE_TARGETDEPS += $$PWD/../../ext/boost/lib/libboost_regex.a
