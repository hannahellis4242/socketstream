/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SocketStream.h"
#include "ServerSocket.h"
#include "ClientSocket.h"
#include "SocketException.h"

SocketStream::SocketStream(Socket *sockPtr, ServerSocket *parent)throw()
{
	parent_=parent;
	socketPtr_.reset(sockPtr);
	streambufPtr_.reset(new SocketStreamBuf(*socketPtr_));
	rdbuf(streambufPtr_.get());
}

SocketStream::SocketStream(const std::string &hostname, const unsigned int port)throw()
	:parent_(nullptr)
{
	if(hostname.empty())
	{
		throw SocketException("hostname must be spesified. If you meant to create a server, use a ServerSocket instead");
	}
	socketPtr_.reset(new ClientSocket(SOCK_STREAM,hostname,port));
	streambufPtr_.reset(new SocketStreamBuf(*socketPtr_));
	rdbuf(streambufPtr_.get());
}

SocketStream::~SocketStream()
{
	if(parent_)
	{
        parent_->disconnect(*socketPtr_);
	}
}

std::string SocketStream::address() const
{
	return socketPtr_->address();
}

unsigned int SocketStream::port() const
{
	return socketPtr_->port();
}

const Socket &SocketStream::getSocket() const
{
	return *socketPtr_;
}
