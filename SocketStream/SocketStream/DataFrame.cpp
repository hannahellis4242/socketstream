#include "DataFrame.h"

DataFrame::DataFrame(const Binary &raw)
{
    fin_=raw[0];
    //extract opcode
    {
        switch(uint8_t(raw.sub(4,7)))
        {
        case 0x00:
            opcode_=CONTINUE;
            break;
        case 0x01:
            opcode_=UFT8;
            break;
        case 0x02:
            opcode_=BIN;
            break;
        case 0x08:
            opcode_=TERMINATE;
            break;
        case 0x09:
            opcode_=PING;
            break;
        case 0x0A:
            opcode_=PONG;
            break;
        }
    }
    masked_=raw[8];
    payloadLength_=raw.sub(9,15);
    auto maskBegin=16u;
    if(payloadLength_==126)
    {
        payloadLength_=raw.sub(16,16+15);
        maskBegin=32u;
    }
    else if(payloadLength_==127)
    {
        payloadLength_=raw.sub(16,16+63);
        maskBegin=16+64;
    }
    maskingKey_=raw.sub(maskBegin,maskBegin+32);
    auto rawPayload=raw.sub(maskBegin+32,raw.bits());
    if(masked_)
    {
        rawPayload^=maskingKey_;
    }
    payload_=convertToString(rawPayload);
}

DataFrame::DataFrame(const std::string &payload, const std::function<Binary()> &maskGenerator)
{
    payloadLength_=payload.size();
    fin_=true;
    masked_=true;
    opcode_=UFT8;
    maskingKey_=maskGenerator();
    payload_=payload;
}

bool DataFrame::lastFrame() const
{
    return fin_;
}

Opcode DataFrame::type() const
{
    return opcode_;
}

const std::string &DataFrame::payload() const
{
    return payload_;
}

Binary DataFrame::raw() const
{
    auto out=Binary(std::vector<bool>(1,fin_));
    out+=Binary(std::vector<bool>(3,false));
    switch(opcode_)
    {
    case CONTINUE:
        out+=Binary(std::vector<bool>(4,false));
        break;
    case UFT8:
        out+=Binary(std::vector<bool>{0,0,0,1});
        break;
    case BIN:
        out+=Binary(std::vector<bool>{0,0,1,0});
        break;
    case TERMINATE:
        out+=Binary(std::vector<bool>{1,0,0,0});
        break;
    case PING:
        out+=Binary(std::vector<bool>{1,0,0,1});
        break;
    case PONG:
        out+=Binary(std::vector<bool>{1,0,1,0});
        break;
    }
    out+=Binary(std::vector<bool>(1,masked_));
    const auto payloadLengthBin=Binary(payloadLength_);
    if(payloadLength_<126)
    {
        out+=payloadLengthBin.sub(64-7,64);
    }
    else if(payloadLength_<0xffff)
    {
        out+=Binary(std::vector<bool>{1,1,1,1,1,1,0});
        out+=payloadLengthBin.sub(64-16,64);
    }
    else
    {
        out+=Binary(std::vector<bool>(6,true));
        out+=payloadLengthBin;
    }
    if(masked_)
    {
        out+=maskingKey_;
    }
    auto payloadBinary=convertToBinary(payload_);
    payloadBinary^=maskingKey_;
    out+=payloadBinary;
    return out;
}
