/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ServerSocket.h"
#include "SocketException.h"
#include "WebSocket.h"
#include <sstream>
#include <netdb.h>
#include <algorithm>

ServerSocket::ServerSocket(const int streamType, const unsigned int port):Socket(::socket(AF_INET,streamType,0))
{
	if(fd_<0)
	{
		throw SocketException("could not obtain socket of desired type");
	}
	auto socketAdd=sockaddr_in();
	socketAdd.sin_family=AF_INET;
	socketAdd.sin_port=htons(port);
	socketAdd.sin_addr.s_addr=INADDR_ANY;

	if(bind(fd_,(struct sockaddr*)&socketAdd, sizeof socketAdd)==-1)
	{
		std::stringstream ss;
		ss<<"could not bind socket to port : "<<port;
		throw SocketException(ss.str());
	}
	//socket is now bound and now we listen for a connection
	auto listenRet=listen(fd_,5);
	if(listenRet==-1)
	{
		std::stringstream ss;
		ss<<"failed to listen on port : "<<port;
		throw SocketException(ss.str());
	}
    add_=*(sockaddr *)(&socketAdd);
}

ServerSocket::~ServerSocket()
{
}

Socket &ServerSocket::accept()
{
	auto connectionSocketAddress=(sockaddr *)nullptr;
	auto connectionSocketAddressLength=(socklen_t)sizeof(sockaddr);
	auto connectedSocketFD=::accept(fd_,connectionSocketAddress,&connectionSocketAddressLength);
	if(connectedSocketFD==-1)
	{
		throw SocketException("failed to accept");
	}
    connections_.push_back(std::unique_ptr<Socket>(new Socket(connectedSocketFD)));
    return *connections_.back();
}

WebSocket &ServerSocket::acceptWebSocket()
{
    auto connectionSocketAddress=(sockaddr *)nullptr;
    auto connectionSocketAddressLength=(socklen_t)sizeof(sockaddr);
    auto connectedSocketFD=::accept(fd_,connectionSocketAddress,&connectionSocketAddressLength);
    if(connectedSocketFD==-1)
    {
        throw SocketException("failed to accept");
    }
    auto out=new WebSocket(connectedSocketFD);
    connections_.push_back(std::unique_ptr<Socket>(out));
    return *out;
}

class SameAddress
{
	private:
        const Socket *ptr_;
	public:
        SameAddress(const Socket &socRef):ptr_(&socRef){}
        bool operator()(const std::unique_ptr<Socket> &uPtr)const
		{
            return uPtr.get()==ptr_;
		}
};

void ServerSocket::disconnect(const Socket &connection)
{
    auto it=std::find_if(connections_.begin(),connections_.end(),SameAddress(connection));
	if(it!=connections_.end())
	{
		connections_.erase(it);
	}
}
