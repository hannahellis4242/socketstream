/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SOCKETSTREAM_H
#define SOCKETSTREAM_H

#include "Socket.h"
#include "ServerSocket.h"
#include "SocketStreamBuf.h"
#include <istream>
#include <memory>

class SocketStream : public std::iostream
{
	private:
		ServerSocket *parent_;
		std::unique_ptr<Socket> socketPtr_;
		std::unique_ptr<SocketStreamBuf> streambufPtr_;

		//disallow copy or assignemnt
		SocketStream(const SocketStream &);
		SocketStream &operator=(const SocketStream &);
	public:
		SocketStream(Socket *sockPtr,ServerSocket *parent=nullptr)throw();
		SocketStream(const std::string &hostname,const unsigned int port)throw();

		~SocketStream();
		std::unique_ptr<SocketStream> accept() const;
		std::string address() const;
		unsigned int port() const;
		const Socket & getSocket() const;
};

#endif // SOCKETSTREAM_H
