#ifndef DATAFRAME_H
#define DATAFRAME_H

#include "Binary.h"
#include <functional>

typedef enum{CONTINUE=0x00,UFT8=0x01,BIN=0x02,TERMINATE=0x08,PING=0x09,PONG=0x0A} Opcode;

class DataFrame
{
    private:
        bool fin_;
        Opcode opcode_;
        bool masked_;
        uint64_t payloadLength_;
        Binary maskingKey_;
        std::string payload_;
    public:
        DataFrame(const Binary &raw);
        DataFrame(const std::string &payload,const std::function<Binary()> &maskGenerator);

        bool lastFrame() const;
        Opcode type() const;
        const std::string &payload() const;
        Binary raw() const;
};

#endif // DATAFRAME_H
