/*
	Copyright (C) 2016  Adam Ellis

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ClientSocket.h"
#include "SocketException.h"
#include <sstream>
#include <netdb.h>
#include <cstring> //for memset
#include <unistd.h> //sockets close

ClientSocket::ClientSocket(const int streamType,const std::string &host,const unsigned int port)throw()
	:Socket(::socket(AF_INET,streamType,0))
{
	auto servinfo=(addrinfo *)nullptr;
	{
		auto hints=addrinfo();
		memset(&hints, 0, sizeof hints);
		hints.ai_family=AF_UNSPEC;
		hints.ai_socktype=SOCK_STREAM;
		auto portString=std::string();
		{
			std::stringstream ss;
			ss<<port;
			portString=ss.str();
		}
		int rv = getaddrinfo(host.c_str(),portString.c_str(), &hints,&servinfo);
		if (rv != 0)
		{
			std::stringstream ss;
			ss<<"getaddrinfo : "<<gai_strerror(rv);
			throw SocketException(ss.str());
		}
	}
	auto connected=false;
	for(auto p=servinfo;p;p=p->ai_next)
	{
		fd_=::socket(p->ai_family,p->ai_socktype,p->ai_protocol);
		if(fd_==-1)
		{
			continue;
		}

		if(connect(fd_,p->ai_addr,p->ai_addrlen)!=-1)
		{
			connected=true;
			add_=*(p->ai_addr);
			break;
		}
		close(fd_);
	}
	if(!connected)
	{
		std::stringstream ss;
		ss<<"could not connect to host : "<<host<<" on port : "<<port;
		throw SocketException(ss.str());
	}
}
