/*
    Copyright (C) 2016  Adam Ellis

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#include "Socket.h"

class WebSocket : public Socket
{
    public:
        WebSocket(const int);
        WebSocket(const WebSocket &)=delete;
        WebSocket & operator=(const WebSocket &)=delete;
        WebSocket(WebSocket &&)=default;
        WebSocket &operator=(WebSocket &&)=default;
        ~WebSocket();
        virtual std::string read(const unsigned int bytes=1024) const;
        virtual void write(const std::string & message) const;
};

#endif // WEBSOCKET_H
