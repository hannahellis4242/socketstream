#ifndef BINARY_H
#define BINARY_H

#include <vector>
#include <stdint.h>
#include <ostream>

class Binary
{
    private:
        std::vector<bool> bin_;
    public:
        Binary()=default;
        Binary(const std::vector<bool> &bin);
        Binary(const char c);
        Binary(const uint8_t c);
        Binary(const uint16_t c);
        Binary(const uint32_t c);
        Binary(const uint64_t c);
        Binary(const int8_t c);
        Binary(const int16_t c);
        Binary(const int32_t c);
        Binary(const int64_t c);

        operator char() const;
        operator uint8_t() const;
        operator uint16_t() const;
        operator uint32_t() const;
        operator uint64_t() const;
        operator int8_t() const;
        operator int16_t() const;
        operator int32_t() const;
        operator int64_t() const;

        bool operator==(const Binary &rhs) const;
        bool operator[](const unsigned int i)const;
        unsigned int bits() const;
        const std::vector<bool> &get()const;
        Binary sub(const unsigned int from,const unsigned int to) const;
        Binary operator+(const Binary &rhs)const;
        const Binary & operator+=(const Binary &rhs);
        const Binary & operator&=(const Binary &rhs);
        const Binary & operator|=(const Binary &rhs);
        const Binary & operator^=(const Binary &rhs);
};

std::ostream &operator<<(std::ostream &os,const Binary &b);

template <class Container>
Binary convertToBinary(typename Container::const_iterator begin,typename Container::const_iterator end)
{
    auto bin=Binary();
    for(auto it=begin;it!=end;++it)
    {
        bin+=Binary(*it);
    }
    return bin;
}

template <class Container>
Binary convertToBinary(const Container &container)
{
    return convertToBinary<Container>(container.begin(),container.end());
}

std::string convertToString(const Binary &bin);

#endif // BINARY_H
